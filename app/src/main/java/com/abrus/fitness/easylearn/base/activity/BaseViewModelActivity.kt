package com.abrus.fitness.easylearn.base.activity

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import com.abrus.fitness.easylearn.base.viewmodel.BaseViewModel
import com.abrus.fitness.easylearn.ui.theme.EasyLearnTheme

abstract class BaseViewModelActivity<T: BaseViewModel>: BaseActivity() {
    abstract val viewModel: T

    abstract val content: @Composable () -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { 
            EasyLearnTheme {
                content()
            }
        }
    }
}