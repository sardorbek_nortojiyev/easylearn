package com.abrus.fitness.easylearn.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.abrus.fitness.easylearn.R

val VesLibre = FontFamily(
    Font(R.font.vesper_libre_italic, FontWeight.Normal),
    Font(R.font.vesper_libre_medium, FontWeight.Medium),
    Font(R.font.vesper_libre_bold, FontWeight.Bold),
)

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = VesLibre,
        fontWeight = FontWeight.Normal,
        color = Color.Black,
        fontSize = 24.sp
    ),
    h1 = TextStyle(
        fontFamily = VesLibre,
        fontWeight = FontWeight.Bold,
        fontSize = 60.sp,
        color = Color.Black
    ),
    subtitle1 = TextStyle(
        fontFamily = VesLibre,
        fontWeight = FontWeight.Medium,
        fontSize = 30.sp,
        color = Color.Black
    ),
    body2 = TextStyle(
        fontFamily = VesLibre,
        fontWeight = FontWeight.Normal,
        fontSize = 10.sp,
        color = Color.Black,
        lineHeight = 16.sp,
//        textDecoration = TextDecoration.Underline
    )
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)