package com.abrus.fitness.easylearn.ui.screens.auth.entrance.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.abrus.fitness.easylearn.R
import com.abrus.fitness.easylearn.ui.components.PrimaryButton
import com.abrus.fitness.easylearn.ui.theme.AppColors
import com.abrus.fitness.easylearn.ui.theme.AppIcons

object EntranceScreen {

    @Composable
    fun Default() {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(AppColors.LightPurple),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                modifier = Modifier
                    .padding(top = 6.dp)) {

                Image(
                    modifier = Modifier
                        .wrapContentSize(),
                    painter = painterResource(AppIcons.Auth.Ellipse),
                    contentDescription = "teacher",
                )

                Text(
                    modifier = Modifier
                        .padding(top = 12.dp),
                    text = stringResource(id = R.string.app_name),
                    style = MaterialTheme.typography.h1,
                    color = AppColors.White
                )
            }

            Image(
                modifier = Modifier
                    .wrapContentSize()
                    .align(Alignment.CenterHorizontally),
                painter = painterResource(AppIcons.Auth.Teacher),
                contentDescription = "teacher",
            )

            Text(
                modifier = Modifier
                .padding(top = 25.dp),
                text = stringResource(id = R.string.app_subtitle),
                style = MaterialTheme.typography.body1,
            )

            Text(
                modifier = Modifier
                    .padding(horizontal = 51.dp, vertical = 12.dp),
                text = stringResource(id = R.string.lorem),
                style = MaterialTheme.typography.body2,
            )

            PrimaryButton.Default(
                modifier = Modifier.padding(horizontal = 76.dp),
                textRes = R.string.enter,
                onClick = { /*TODO*/ },
                background = AppColors.MainColor)

            Row(
                modifier = Modifier
                    .padding(top = 6.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.authentication),
                    style = MaterialTheme.typography.body2,
                )

                Image(
                    modifier = Modifier
                        .wrapContentSize(),
                    painter = painterResource(AppIcons.Auth.Google),
                    contentDescription = "google",
                )
            }
        }
    }
}
