package com.abrus.fitness.easylearn.ui.theme

import com.abrus.fitness.easylearn.R

object AppIcons {

    object Auth{
        const val Teacher = R.drawable.ic_teacher
        const val Ellipse = R.drawable.ic_blue_ellipse
        const val Google = R.drawable.ic_google
    }
}