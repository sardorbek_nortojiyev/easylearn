package com.abrus.fitness.easylearn.base.viewmodel

import androidx.lifecycle.ViewModel
import com.abrus.fitness.easylearn.base.state.BaseState

abstract class BaseViewModel: ViewModel() {

    open val baseState = BaseState()
}