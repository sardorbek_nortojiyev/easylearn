package com.abrus.fitness.easylearn.ui.screens.auth.entrance

import android.annotation.SuppressLint
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import com.abrus.fitness.easylearn.base.activity.BaseViewModelActivity
import com.abrus.fitness.easylearn.ui.screens.auth.entrance.screen.EntranceScreen
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class EntranceActivity : BaseViewModelActivity<EntranceViewModel>(){

    override val viewModel: EntranceViewModel by viewModels()
    override val content: @Composable () -> Unit = {
        EntranceScreen.Default()
    }

    override fun initialize() {

    }

}