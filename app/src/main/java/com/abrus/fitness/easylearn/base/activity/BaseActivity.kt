package com.abrus.fitness.easylearn.base.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

abstract class BaseActivity: ComponentActivity() {
    protected val uiScope = CoroutineScope(Dispatchers.Main.immediate)

    abstract fun initialize()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initialize()
    }
}