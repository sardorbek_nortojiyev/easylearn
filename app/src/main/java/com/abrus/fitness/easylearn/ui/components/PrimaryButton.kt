package com.abrus.fitness.easylearn.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.abrus.fitness.easylearn.ui.theme.AppColors

object PrimaryButton {

    @Composable
    fun Default(
        modifier: Modifier = Modifier,
        textRes: Int,
        onClick: () -> Unit,
        background: Color,
        isActive: Boolean = true
    ) {
        Box(
            modifier = modifier
                .fillMaxWidth()
                .height(50.dp)
                .clip(RoundedCornerShape(20.dp))
                .alpha(if (isActive) 1f else 0.5f)
                .background(background)
                .clickable { if (isActive) onClick() },
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = stringResource(id = textRes),
                style = MaterialTheme.typography.subtitle1,
                color = AppColors.Black
            )
        }
    }
}