package com.abrus.fitness.easylearn.ui.theme

import android.annotation.SuppressLint
import androidx.compose.ui.graphics.Color


@SuppressLint("InvalidColorHexValue")
object AppColors{
    val LightPurple = Color(0xFFFCDDEC)
    val Black = Color(0xFF000000)
    val White = Color(0xFFFFFFFF)
    val MainColor = Color(0xFF6EB7B7)
    val DarkGreen = Color(0x6E99DCDC)
    val LightBlack = Color(0xFF1E1E1E)
}